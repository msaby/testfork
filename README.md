# Data Librarianship - LIS 628

[![forthebadge](https://forthebadge.com/images/badges/contains-cat-gifs.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/cc-sa.svg)](https://forthebadge.com)

## About
LIS 628 in Pratt SLIS. 

The world of data is seemingly a new frontier for libraries, yet in some ways, data and data sets are comparable to other print and electronic resources that librarians historically have been charged with locating, teaching, collecting, organizing, and preserving. This course asks how best we can serve the needs of a burgeoning community of data users/producers while meeting the new challenges that data present to our existing skillsets, workflows, and infrastructure. Topics will include data reference and literacy; archives and repositories; formats and standards; ethics and policy. Statistical/GIS software and research data management are also explored.

## Build our book locally
1. Fork, clone or download this project (See #1-4 in the 'Contribute!' section below)
2. Install R & RStudio
3. Install the bookdown, RMarkdown, and tinytex packages in RStudio with the following two commands in the R terminal:
	* `install.packages(c("rmarkdown", "bookdownplus", "tinytex"))`
	* `tinytex::install_tinytex()`
	You can also click Tools > Install Packages and type the package names (make sure "install dependencies" is checked) separated by commas.
4. Go to the project folder and click `data-librarianship.Rproj`
5. Run this command in the R terminal: `bookdown::render_book('index.Rmd', 'all')`
6. Go to the folder `_book` in the project folder and click `index.html` to view the book locally in your browser.

## Contribute!
If you'd like to contribute, that would be amazing! I will try my best to be on top of merge requests and issues. Beforey you start, please refer to our [contributing guide](CONTRIBUTING.md). 

## Contact info
You are welcome to email me at [vsteeves at pratt dot edu](mailto:vsteeves@pratt.edu) if you have questions or concerns, or raise an issue on this repository and I will do my best to respond quickly!
