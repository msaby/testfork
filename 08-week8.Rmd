# Data services in libraries {#data-srvcs-lib}

![](imgs/lb-services.png)

Before we start, I wanted to draw everyone's attention to two changes in the syllabus:

+ You now have 10 homework assignments instead of 12. Each is word 3% of your final grade, 30% total.
+ Participation is now at 20% to make the total graded work 100%.

## Discussion Questions {#services-discussion-questions}

1. **GOBEN-ZILINSKI-BRINEY**: The article "*Going Beyond the Data Management Plan*" provided an overview of possible data management services that could be provided by either a library or through collaboration with other institutional entities. Some of these potential services we are well acquainted with - assistance in helping patrons find data, for example - but we were also introduced to some new potential services. 
    + Which of the services you read about that you hadn't considered before would be of the greatest interest to you if you were a librarian at a well-funded institution?
    + Which of those services would be better suited to a smaller institution with less institutional buy-in for providing data management services?
2. **SALO**: "*Retooling Libraries for the Data Challenge*" outlines how the "buzzworthy" requirement of data management needs to be considered as a task in and of itself, rather than as an extension or transformation of the capabilities of digital libraries and institutional repositories, and then offers a selection of "ways forward". 
    + What are most accessible (lowest-hanging fruit) options put forth within the "ways forward" section? 
    + Do you think ‘data management' is buzzworthy / hyped too much / hyped too little in the current LIS discourse?
3. **EMMELHAIN**: Emmelhain posits that academic libraries are in the business of helping patrons understand and providing access to research data, while public libraries are in the business of helping patrons understand and providing access to open data. 
    + (Although I recognize the helpfulness in simplifying the differences between the two for the purposes of writing this article), is the "academic/research data" vs. "public/open data" dichotomy that Emmelhain presents true? 
    + What are ways that we can rethink the presentation of data services to patrons in a public library setting that shifts away from thinking of data services as a "new" offering, thereby reducing the potential anxiety of library staff at the thought of having to add something else to their (probably already overflowing) plate? 
        - I ask this question with the caveat and knowledge that while re-framing a task can help individuals adjust to a changing workload, it doesn't actually reduce the workload itself.
4. **COATES**: "Building enduring collaborative relationships with researchers is an important skill set that is poorly documented." Many of our readings from this and previous weeks have talked about the importance of institutional buy-in and cross-entity collaboration for providing data services, but few have touched upon the details of that type of "unseen" work in creating and maintaining relationships. 
    + One possible goal of data management is to document as you go rather than document after the fact. Is there a way that the work of relationship building can be better documented in this context building community support for providing data management services? Is it even possible to document?
    + Bonus Question: What were your thoughts on the "top-down" vs. "bottom-up" approaches presented?
    
## Lecture {#services-lecture}
So this is the first lecture of unit 2! Unit 1 was focused on more of the "on-the-ground" challenges of every day patrons from various domains/walks of life/areas of study. Unit 2 will focusing on how we can build and provide library services for data broadly -- from collecting data and giving access, to data reference, to data

So today we are going to talk about how to build data services! While there's no one-size-fits-all, there are best practices and tips that can help get institutional and community buy-in, resource and capacity building, and collaborations/emotional labor.

We talked about the breadth of data types out there, and Dorothea Salo did a great job of underscoring that in her article that we read for this week. One part I would tease out here is that the variability of the work done with data (in addition to the variability of data itself) requires that the first thing to do before building *any* type of service is to conduct an **internal and external environmental scan**. In fact, Dorothea mentions that in the conclusion of her article \& it's completely on point:

> However, unless we proceed with clear understanding of researchers and their data, as well as our own systems and habits, we will simply trip over ourselves. Research data are too important, and our role in curating them at present too insecure, to allow that to happen.

So let's talk about the ways that we can understand the way that our communities are using, creating, analyzing, storing, and generally dealing with their data! We can do that in a few key ways.

*External environment* scans involve analyzing opportunities and threats to an institution/service via the national environment, broader socio-economic environment, and industry environment. An example of this might be looking at the national requirements for DMPs and then searching for services that address it in similar institutions to make a case for adding services to your own institution.

> At NYU, my job came out of an external environmental scan. IT and the Libraries does a joint benchmarking endeavor once every few years that looks at things like AI/machine learning, data management, and other areas where we might expand. We compare grant requirements, other services at similar institutions (R1s), and infrastructure capabilities. They found that most R1s had data management as a service, and so my job was created!

An *internal environment scan* is a type of requirements gathering process where you look at the present capabilities/limits of your organization (infrastructure, hardware, personnel, abilities, structure, etc.) and compare the findings to what the organization will **need** in the future to achieve its strategic goals or fill gaps in service offerings (like data services!).

> In my first year, this was pretty much all I did. I met with every single subject liaison to get an idea of the research done in their departments, and how I could best make services and offerings that would fill a capacity gap or be the most immediately impactful. After getting some baseline from my colleagues in the library, I did a lot of outreach to students, staff, and faculty around the university to tell them that this service exists! And we're here to help. That was **a lot** of my first year building the RDM service at NYU.

Observing the internal organizational environment includes employee interaction with other employees/staff/faculty/students/curators/etc., interaction with management, manager interaction with other managers, inter-unit collaboration, access to resources, service awareness, organizational structure, operational potential, and sometimes even doing some human-computer interaction stuff. Is the current research infrastructure usable? Watching students try to go through their giant nest of files on the lab server was a great way to find new avenues to build in teachable moments, or build out services that could help them.

| Scanning Type | General Characteristics | Pros | Cons |
|---------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Ad-hoc |  usually in response to a crisis <br/> usually not in-depth analysis <br/> findings are more short-term |  quicker turnaround of results <br/> lower commitment of resources over time |  data is more superficial <br/> results are less generalization <br/> more reactive than proactive, can't really do any forward-thinking with this method always catching up, never ahead |
| Periodic |  tied to a planning cycle (e.g. every 5 years to align with strategic planning) <br/> usually in-depth <br/> analysis findings usually last another 5 - 10 years | predictable frequency <br/> lets us plan a budget easier <br/> information is typically very timely <br/> planning is more proactive | reaction to unforeseen environmental changes may require an additional ad-hoc scan planning response to changes is more reactive than proactive |
| Continuous | structured, very in-depth data collection/analysis <br/> dedicated staff to doing this <br/> data collection is very comprehensive |  dedicated staff means more data from more sources provides folks in power/decision-makers with more information <br/> plans can be adapted/changed/stopped more proactively |  requires a lot of money/resources/institutional buy-in |

A lot of the time, folks use surveys, interviews, and focus groups for internal scans, and surveys, literature/document, and calling/emailing your colleagues at other institutions for external scans. There is a great existing tool to facilitate internal scans, and some public iterations/outcomes from this tool that can serve as a great dataset for an external review (doc analysis):

+ [Digital Curation Profiles](http://datacurationprofiles.org/): "A Data Curation Profile is essentially the 'story' of a data set or collection, describing its origin and life cycle within a research project. The Profile and its associated Toolkit grew out of an inquiry into the changing environment of scholarly communication, especially the possibility of researchers providing access to data much further upstream than previously imagined: If researchers are interested in sharing or required to provide access to data sets or collections, what does that mean for the data, for researchers, and for librarians?"
    - I use the underlying questions as the basis for a lot of interviews that I did with faculty -- so I didn't make a profile with it, but I used the same questions to get at the heart of the research process when conducting internal environmental scans
    - see article: "[Using Data Curation Profiles to Design the Datastar Dataset Registry](http://www.dlib.org/dlib/july13/wright/07wright.html)"

Once you have conducted your internal and external scanning, you are ready to build your service! What I tell folks who are just starting out, or maybe don't have the resources to start building a service that they can devote all their time to (I know many librarians who part-time can maybe fit in data service activities into their schedule because of resource issues!), is to just start by reviewing data management plans. Patrons email me their DMPs, and I mark them up and send them back. Sometimes we have an accompanying phone call or meeting depending on the scale and type of study (e.g. the data needs extra security, or it's a 3-institution collaboration). I basically go over the plans and make sure they are hitting the best practices -- are the formats open? Do they have a plan for backups and data sharing?

> Recently, the federal agencies have been sending back grants to PIs because of an insufficient data management plan. In fact, this happened to a number of researchers last year who I provide the service for! This is something I've noted in my annual service review (which includes other facets of environmental scanning and experiences with patrons) that goes to the higher-ups in my library.

Growing services often involve reaching out to the larger community for collaboration and general outreach about potential new service offerings (e.g. pilots, preliminary workshops, user testing, etc.). One other point that we discussed in our questions portion of class and one thing that we saw across readings was collaboration and relationship building with other units in an institution, such as:

+ IT
+ Institutional Review Board (review human/animal subjects research)
+ Office of Sponsored Programs (grant admin folks)
+ Faculty & Student Senate (if at university)
+ Labs (e.g. SMaPP)
+ Centers (e.g. CUSP & CDS)

Goben, Zilinski, Briney (2016) in particular outline cross-unit collaboration for data services as a good way to build services from the ground-up:

> While a skilled librarian may be able to deliver very specialized services, it is often more sustainable to have several individuals providing at least low-level data support. A group approach also enables data support to be a normalized part of the total library offerings. Additionally, partnering with other campus offices could allow for resource sharing of both personnel and budget.

While these collaborations are incredibly fruitful when done with mutual boundaries, respect, and understanding, this is a lot of emotional labor. It's very hard work to pursue and build relationships, to manage conflicting ideas. There are loads of cultural barriers to new data services in libraries. Some things I've heard from researchers at many different institutions:

+ Isn't this an IT thing?
+ How do you know anything about data?
+ You can't possibly understand *my* data and *my* research!

These cross-unit collaborations can also help get your feet in the door of many who would share these biases.

No matter what route you take in gathering resources and staffing, as the need is continually assessed and the services grow, you can provide data services through a number of avenues within your institution. This image explains the four most common I've seen:

![Most common types of data services](imgs/dataservices.png)

Coates also mentions a few key services in her article not seen in the diagram above (but could be classified in it!):

+ data reference and guides
+ institutional repositories (IR)
+ purchasing, acquisition, and licensing data
+ data citation
+ metadata and standards support
+ embedded librarians as data managers
+ working to influence/create policies around data within institution & externally
+ promoting open data

I can't say enough, that it's all about *gradual* ramping up of services in practice. Ideally, you'd have all these services, and research was being well-managed and preserved from the very start. This just is never grounded in reality. You need to do community building, gather resources for infrastructure, hire people (one person cannot do this job for a whole campus), and you'll likely try a lot of things that fail at first.

> This "Build-Measure-Learn" (*Lean Startup Method*, Eric Ries 2014)  approach comes from the lean startup movement and emphasizes continuous innovation. The ideais that small, early failures produce a better product  in  the  end. Adopting this iterative and flexible approach can help data services remain relevant in a rapidly changing research environment. -- Coates (2014)

## First check-in!

Please submit the materials for the first check-in via this link: https://cloud.remram.fr/s/ASf57EManjcLJfp before 11:55pm on Thursday the 18th.

The order for the in-class presentations:

1. Sarah
2. Willamae
3. Robin
4. <s>Genevieve</s>
5. Abigail